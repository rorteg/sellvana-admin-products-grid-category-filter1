<?php
/*
# NOTICE OF LICENSE
#
# This source file is subject to the Open Software License (OSL 3.0)
# that is available through the world-wide-web at this URL:
# http://opensource.org/licenses/osl-3.0.php
#
# @category    Vuleticd
# @package     Vuleticd_AdminGridCategoryFilter
# @copyright   Copyright (c) 2014 Vuletic Dragan (http://www.vuleticd.com)
# @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/
class Vuleticd_AdminGridCategoryFilter_Admin
{
    public static function onGridView($args)
    {
        $view = $args['view'];

        $grid = $view->getGrid();
        $orm = $grid['config']['orm'];

        $tProdAlias = $orm->table_alias();
        $tCat = FCom_Catalog_Model_Category::table();
        $tCatProd = FCom_Catalog_Model_CategoryProduct::table();

        $catColSql = "(
            SELECT GROUP_CONCAT(DISTINCT c.full_name SEPARATOR ', ') categories
            FROM {$tCat} c
            INNER JOIN {$tCatProd} cp ON cp.category_id=c.id
            WHERE cp.product_id={$tProdAlias}.id
        )";

        // Add SQL columns
        $orm->select($catColSql, 'categories');

        // Add grid columns
        $grid['config']['columns'][] = array('name'=>'categories', 'label'=>'In Category', 'index' => $catColSql);
        // Add grid filters
        $grid['config']['filters'][] = array('field'=>'categories', 'type'=>'text');

        $view->set('grid', $grid);

    }

}
